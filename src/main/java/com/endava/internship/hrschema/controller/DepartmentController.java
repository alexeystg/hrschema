package com.endava.internship.hrschema.controller;


import com.endava.internship.hrschema.facade.DepartmentFacade;
import com.endava.internship.hrschema.model.request.DepartmentCreateRequest;
import com.endava.internship.hrschema.model.request.DepartmentUpdateRequest;
import com.endava.internship.hrschema.service.DepartmentValidationService;
import com.endava.internship.hrschema.utils.department.DepartmentConverter;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.endava.internship.hrschema.utils.department.DepartmentConverter.*;


@AllArgsConstructor
@RestController
@RequestMapping("/api/departments")
public class DepartmentController {

    private final DepartmentFacade departmentFacade;
    private final DepartmentValidationService departmentValidationService;


    @GetMapping
    public ResponseEntity<List<Object>> getAllDepartments(@RequestParam(required = false) boolean expand) {
        if (expand) {
            return ResponseEntity.ok()
                    .body(departmentFacade.getAllDepartments().stream()
                            .map(DepartmentConverter::convertFromDtoToExpandedResponse)
                            .collect(Collectors.toList()));
        }
        return ResponseEntity.ok()
                .body(departmentFacade.getAllDepartments().stream()
                        .map(DepartmentConverter::convertFromDtoToResponse)
                        .collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getDepartmentById(@PathVariable Long id,
                                                    @RequestParam(required = false) boolean expand) {
        if (expand) {
            return ResponseEntity.ok().
                    body(convertFromDtoToExpandedResponse(departmentFacade.getDepartmentById(id)));
        }
        return ResponseEntity.ok()
                .body(convertFromDtoToResponse(departmentFacade.getDepartmentById(id)));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping()
    public void createDepartment(@RequestBody DepartmentCreateRequest department) {
        departmentValidationService.validateDepartment(department);
        departmentFacade.createDepartment(convertFromCreateRequestToDto(department));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{id}")
    public void updateDepartment(@RequestBody DepartmentUpdateRequest department,
                                 @PathVariable Long id) {
        departmentValidationService.validateDepartment(department);
        departmentFacade.updateDepartment(convertFromUpdateRequestToDto(department), id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteDepartmentById(@PathVariable Long id) {
        departmentFacade.deleteDepartment(id);
    }
}
