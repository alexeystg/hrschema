package com.endava.internship.hrschema.controller;

import com.endava.internship.hrschema.facade.EmployeeFacade;
import com.endava.internship.hrschema.model.request.EmployeeCreateRequest;
import com.endava.internship.hrschema.model.request.EmployeeUpdateRequest;
import com.endava.internship.hrschema.service.EmployeeValidationService;
import com.endava.internship.hrschema.utils.employee.EmployeeConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/employees")
public class EmployeeController {
    private final EmployeeFacade employeeFacade;
    private final EmployeeValidationService employeeValidationService;

    @GetMapping
    public ResponseEntity<List<Object>> getAllEmployees(@RequestParam(required = false) boolean expand) {
        if (expand) {
            return ResponseEntity.ok().body(employeeFacade.getAllEmployees().stream()
                    .map(EmployeeConverter::convertFromDtoToExpandedResponse)
                    .collect(toList()));
        }
        return ResponseEntity.ok().body(employeeFacade.getAllEmployees().stream()
                .map(EmployeeConverter::convertFromDtoToResponse)
                .collect(toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getEmployeeById(
            @PathVariable Long id,
            @RequestParam(required = false) boolean expand
    ) {
        if (expand) {
            return ResponseEntity.ok()
                    .body(EmployeeConverter.convertFromDtoToExpandedResponse(employeeFacade.getEmployeeById(id)));
        }
        return ResponseEntity.ok()
                .body(EmployeeConverter.convertFromDtoToResponse(employeeFacade.getEmployeeById(id)));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping()
    public void addNewEmployee(@RequestBody EmployeeCreateRequest employeeCreateRequest) {
        employeeValidationService.validateEmployee(employeeCreateRequest);
        employeeFacade.createNewEmployee(EmployeeConverter.convertFromCreateRequestToDto(employeeCreateRequest));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{id}")
    public void updateEmployee(
            @RequestBody EmployeeUpdateRequest employeeUpdateRequest,
            @PathVariable Long id
    ) {
        employeeValidationService.validateEmployee(employeeUpdateRequest);
        employeeFacade.updateEmployee(EmployeeConverter.convertFromUpdateRequestToDto(employeeUpdateRequest), id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteEmployeeById(@PathVariable Long id) {
        employeeFacade.deleteEmployee(id);
    }


}
