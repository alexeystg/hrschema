package com.endava.internship.hrschema.facade;

import com.endava.internship.hrschema.model.dto.EmployeeDto;
import com.endava.internship.hrschema.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class EmployeeFacade {

    private final EmployeeService employeeService;

    public List<EmployeeDto> getAllEmployees() {
        return employeeService.getAllEmployees();
    }

    public EmployeeDto getEmployeeById(Long id) {
        return employeeService.getEmployeeById(id);
    }

    public void createNewEmployee(EmployeeDto employee) {
        employeeService.createNewEmployee(employee);
    }

    public void updateEmployee(EmployeeDto employeeDto, Long id) {
        employeeService.updateEmployee(employeeDto, id);
    }

    public void deleteEmployee(Long id) {
        employeeService.deleteEmployee(id);
    }
}
