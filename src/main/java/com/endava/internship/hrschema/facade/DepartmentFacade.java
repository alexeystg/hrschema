package com.endava.internship.hrschema.facade;

import com.endava.internship.hrschema.model.dto.DepartmentDto;
import com.endava.internship.hrschema.service.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DepartmentFacade {

    private final DepartmentService service;

    public DepartmentDto getDepartmentById(Long id) {
        return service.getDepartmentById(id);
    }

    public List<DepartmentDto> getAllDepartments() {
        return service.getAllDepartments();
    }

    public void createDepartment(DepartmentDto departmentDto) {
        service.createNewDepartment(departmentDto);
    }

    public void updateDepartment(DepartmentDto departmentDto, Long id) {
        service.updateDepartment(departmentDto, id);
    }

    public void deleteDepartment(Long id) {
        service.deleteDepartment(id);
    }
}
