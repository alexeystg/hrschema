package com.endava.internship.hrschema.model.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Data
@Entity
@Table(name = "employees")
@DynamicUpdate
public class EmployeeEntity {

    @Id
    @Column(name = "employee_id")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "employee_id_seq")
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email")
    private String email;
    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "hire_date")
    private String hireDate;
    @Column(name = "job_id")
    private String job;
    @Column(name = "salary")
    private Double salary;
    @Column(name = "commission_pct")
    private Float commissionPct;
    @Nullable
    @JoinColumn(name = "manager_id", referencedColumnName = "employee_id")
    @ManyToOne
    private EmployeeEntity managerId;
    @Nullable
    @JoinColumn(name = "department_id")
    @ManyToOne
    private DepartmentEntity departmentId;

}