package com.endava.internship.hrschema.model.request;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class DepartmentUpdateRequest {
    String departmentName;
    Long managerId;
    Long locationId;
}
