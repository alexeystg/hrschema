package com.endava.internship.hrschema.model.response;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeResponse {

    private String firstName;
    private String lastName;
    private Long departmentId;
}
