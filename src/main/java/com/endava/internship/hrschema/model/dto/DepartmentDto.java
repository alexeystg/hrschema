package com.endava.internship.hrschema.model.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Value;

@Data
@Builder
public class DepartmentDto {
    Long departmentId;
    String departmentName;
    Long managerId;
    Long locationId;
}
