package com.endava.internship.hrschema.model.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Value;

@Data
@Builder
public class EmployeeDto {

    Long id;
    String firstName;
    String lastName;
    String email;
    String phoneNumber;
    String hireDate;
    String job;
    Double salary;
    Float commissionPct;
    Long managerId;
    Long departmentId;
}
