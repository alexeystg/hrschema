package com.endava.internship.hrschema.model.request;


import lombok.Builder;
import lombok.Value;


@Value
@Builder
public class EmployeeCreateRequest {

    String firstName;
    String lastName;
    String email;
    String phoneNumber;
    String hireDate;
    String job;
    Double salary;
    Float commissionPct;
    Long managerId;
    Long departmentId;
}
