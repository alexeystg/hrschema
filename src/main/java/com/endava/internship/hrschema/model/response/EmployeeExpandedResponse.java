package com.endava.internship.hrschema.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeExpandedResponse {

    String firstName;
    String lastName;
    String email;
    String phoneNumber;
    String hireDate;
    String job;
    Double salary;
    Float commissionPct;
    Long managerId;
    Long departmentId;
}
