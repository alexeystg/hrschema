package com.endava.internship.hrschema.model.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Data
@Entity
@Table(name = "departments")
@DynamicUpdate
public class DepartmentEntity {
    @Id
    @Column(name = "department_id")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "departments_id_seq")
    private Long id;
    @Column(name = "department_name")
    private String departmentName;
    @Nullable
    @JoinColumn(name = "manager_id", referencedColumnName = "employee_id")
    @ManyToOne
    private EmployeeEntity managerId;
    @Column(name = "location_id")
    private Long locationId;

}
