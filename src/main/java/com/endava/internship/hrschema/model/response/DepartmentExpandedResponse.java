package com.endava.internship.hrschema.model.response;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentExpandedResponse {
    String departmentName;
    Long managerId;
    Long locationId;
}
