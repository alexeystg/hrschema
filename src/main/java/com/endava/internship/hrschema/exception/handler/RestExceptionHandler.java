package com.endava.internship.hrschema.exception.handler;


import com.endava.internship.hrschema.exception.ExceptionType;
import com.endava.internship.hrschema.exception.HrSchemaException;
import com.endava.internship.hrschema.model.response.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler {


    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleHrSchemaException(HrSchemaException ex) {
        ExceptionType exceptionType = ex.getExceptionType();
        return ResponseEntity.status(exceptionType.getStatus())
                .body(new ErrorResponse(
                        exceptionType.name(),
                        exceptionType.getMessage()
                ));
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleUnknownException(
            RuntimeException ex) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ErrorResponse(
                        "Unexpected exception",
                        "Unexpected error occurred"
                ));
    }
}
