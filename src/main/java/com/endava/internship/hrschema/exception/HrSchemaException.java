package com.endava.internship.hrschema.exception;

import lombok.Getter;

@Getter
public class HrSchemaException extends RuntimeException {

    private final ExceptionType exceptionType;

    private HrSchemaException(ExceptionType exceptionType) {
        this.exceptionType = exceptionType;
    }

    public static HrSchemaException of(ExceptionType exceptionType) {
        return new HrSchemaException(exceptionType);
    }
}
