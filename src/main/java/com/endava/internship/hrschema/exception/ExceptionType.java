package com.endava.internship.hrschema.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ExceptionType {

    EMPLOYEE_NOT_FOUND("Employee was not found", 404),
    DEPARTMENT_NOT_FOUND ("Department was not found", 404),
    INVALID_FIRST_NAME("First name must not be null or empty", 400),
    INVALID_LAST_NAME("Last name must not be null or empty", 400),
    INVALID_EMAIL("Email must be valid", 400),
    INVALID_PHONE_NUMBER("Phone number must be exactly 9 digits starting with a 0", 400),
    INVALID_JOB("Job must not be null or empty", 400),
    INVALID_SALARY("Salary must be greater than 1", 400),
    INVALID_DEPARTMENT_NAME("Department name must not be null or empty", 400),
    INVALID_LOCATION_ID("Location id must not be null", 400),
    INTERNAL_SERVER_ERROR("Unexpected exception", 500);


    private final String message;
    private final int status;

}
