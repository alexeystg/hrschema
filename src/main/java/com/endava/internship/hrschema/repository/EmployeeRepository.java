package com.endava.internship.hrschema.repository;

import com.endava.internship.hrschema.model.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {
}
