package com.endava.internship.hrschema.repository;

import com.endava.internship.hrschema.model.entity.DepartmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface DepartmentRepository extends JpaRepository<DepartmentEntity, Long> {
}
