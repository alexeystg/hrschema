package com.endava.internship.hrschema.service;

import com.endava.internship.hrschema.model.dto.EmployeeDto;

import java.util.List;

public interface EmployeeService {

    List<EmployeeDto> getAllEmployees();

    EmployeeDto getEmployeeById(Long id);

    void createNewEmployee(EmployeeDto employeeDto);

    void updateEmployee(EmployeeDto employeeDto, Long id);

    void deleteEmployee(Long id);
}
