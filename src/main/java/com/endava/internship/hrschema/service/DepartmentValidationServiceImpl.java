package com.endava.internship.hrschema.service;

import com.endava.internship.hrschema.exception.HrSchemaException;
import com.endava.internship.hrschema.model.request.DepartmentCreateRequest;
import com.endava.internship.hrschema.model.request.DepartmentUpdateRequest;
import org.springframework.stereotype.Service;

import static com.endava.internship.hrschema.exception.ExceptionType.INVALID_DEPARTMENT_NAME;
import static com.endava.internship.hrschema.exception.ExceptionType.INVALID_LOCATION_ID;

@Service
public class DepartmentValidationServiceImpl implements DepartmentValidationService {

    public void validateDepartment(DepartmentCreateRequest request){
        validateDepartmentName(request.getDepartmentName());
        validateLocationId(request.getLocationId());
    }

    public void validateDepartment(DepartmentUpdateRequest request){
        validateDepartmentName(request.getDepartmentName());
        validateLocationId(request.getLocationId());
    }

    private void validateDepartmentName(String departmentName) {
        if (departmentName == null || departmentName.isEmpty()){
            throw HrSchemaException.of(INVALID_DEPARTMENT_NAME);
        }
    }

    private void validateLocationId(Long locationId) {
        if (locationId == null){
            throw HrSchemaException.of(INVALID_LOCATION_ID);
        }
    }
}
