package com.endava.internship.hrschema.service;


import com.endava.internship.hrschema.model.request.DepartmentCreateRequest;
import com.endava.internship.hrschema.model.request.DepartmentUpdateRequest;

public interface DepartmentValidationService {

    void validateDepartment(DepartmentCreateRequest request);

    void validateDepartment(DepartmentUpdateRequest request);

}
