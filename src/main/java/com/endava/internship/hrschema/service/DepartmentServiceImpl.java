package com.endava.internship.hrschema.service;


import com.endava.internship.hrschema.exception.HrSchemaException;
import com.endava.internship.hrschema.model.dto.DepartmentDto;
import com.endava.internship.hrschema.model.entity.DepartmentEntity;
import com.endava.internship.hrschema.repository.DepartmentRepository;
import com.endava.internship.hrschema.utils.department.DepartmentConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.endava.internship.hrschema.exception.ExceptionType.DEPARTMENT_NOT_FOUND;

@RequiredArgsConstructor
@Service
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<DepartmentDto> getAllDepartments() {
        return repository.findAll().stream()
                .map(DepartmentConverter::convertFromEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public DepartmentDto getDepartmentById(Long id) {
        return repository.findById(id)
                .map(DepartmentConverter::convertFromEntityToDto)
                .orElseThrow(() -> HrSchemaException.of(DEPARTMENT_NOT_FOUND));
    }

    @Override
    @Transactional
    public void createNewDepartment(DepartmentDto departmentDto) {
        repository.save(DepartmentConverter.convertFromDtoToEntity(departmentDto));
    }

    @Override
    @Transactional
    public void updateDepartment(DepartmentDto department, Long id) {
        DepartmentEntity departmentEntity = DepartmentConverter.convertFromDtoToEntity(department);
        departmentEntity.setId(id);
        repository.save(departmentEntity);
    }

    @Override
    @Transactional
    public void deleteDepartment(Long id) {
        repository.deleteById(id);
    }

}
