package com.endava.internship.hrschema.service;

import com.endava.internship.hrschema.exception.HrSchemaException;
import com.endava.internship.hrschema.model.request.EmployeeCreateRequest;
import com.endava.internship.hrschema.model.request.EmployeeUpdateRequest;
import org.springframework.stereotype.Service;

import static com.endava.internship.hrschema.exception.ExceptionType.*;

@Service
public class EmployeeValidationServiceImpl implements EmployeeValidationService {

    private static final String EMAIL_REGEX = "^[^@\\s]+@[^@\\s\\.]+\\.[^@\\.\\s]+$";
    private static final String PHONE_NUMBER_REGEX = "0+\\d{8}";

    public void validateEmployee(EmployeeCreateRequest request) {
        validateFirstName(request.getFirstName());
        validateLastName(request.getLastName());
        validateEmail(request.getEmail());
        validatePhoneNumber(request.getPhoneNumber());
        validateJob(request.getJob());
        validateSalary(request.getSalary());
    }

    public void validateEmployee(EmployeeUpdateRequest request) {
        validateFirstName(request.getFirstName());
        validateLastName(request.getLastName());
        validateEmail(request.getEmail());
        validatePhoneNumber(request.getPhoneNumber());
        validateJob(request.getJob());
        validateSalary(request.getSalary());
    }

    private void validateFirstName(String firstName) {
        if (firstName == null || firstName.isEmpty()) {
            throw HrSchemaException.of(INVALID_FIRST_NAME);
        }
    }

    private void validateLastName(String lastName) {
        if (lastName == null || lastName.isEmpty()) {
            throw HrSchemaException.of(INVALID_LAST_NAME);
        }
    }

    private void validateEmail(String email) {
        if (!email.matches(EMAIL_REGEX)) {
            throw HrSchemaException.of(INVALID_EMAIL);
        }
    }

    private void validatePhoneNumber(String phoneNumber) {
        if (!phoneNumber.matches(PHONE_NUMBER_REGEX)) {
            throw HrSchemaException.of(INVALID_PHONE_NUMBER);
        }
    }

    private void validateJob(String job) {
        if (job == null || job.isEmpty()) {
            throw HrSchemaException.of(INVALID_JOB);
        }
    }

    private void validateSalary(Double salary) {
        if (salary < 1.0) {
            throw HrSchemaException.of(INVALID_SALARY);
        }
    }
}
