package com.endava.internship.hrschema.service;

import com.endava.internship.hrschema.exception.HrSchemaException;
import com.endava.internship.hrschema.model.dto.EmployeeDto;
import com.endava.internship.hrschema.model.entity.EmployeeEntity;
import com.endava.internship.hrschema.repository.EmployeeRepository;
import com.endava.internship.hrschema.utils.employee.EmployeeConverter;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.endava.internship.hrschema.exception.ExceptionType.EMPLOYEE_NOT_FOUND;

@Service
@AllArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<EmployeeDto> getAllEmployees() {
        return repository.findAll().stream()
                .map(EmployeeConverter::convertFromEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public EmployeeDto getEmployeeById(Long id) {
        return repository.findById(id)
                .map(EmployeeConverter::convertFromEntityToDto)
                .orElseThrow(() -> HrSchemaException.of(EMPLOYEE_NOT_FOUND));
    }


    @Override
    @Transactional
    public void createNewEmployee(EmployeeDto employeeDto) {
        repository.save(EmployeeConverter.convertFromDtoToEntity(employeeDto));
    }

    @Override
    @Transactional
    public void updateEmployee(EmployeeDto employeeDto, Long id) {
        EmployeeEntity employeeEntity= EmployeeConverter.convertFromDtoToEntity(employeeDto);
        employeeEntity.setId(id);
        repository.save(employeeEntity);
    }

    @Override
    @Transactional
    public void deleteEmployee(Long id) {
        repository.deleteById(id);
    }
}
