package com.endava.internship.hrschema.service;


import com.endava.internship.hrschema.model.dto.DepartmentDto;

import java.util.List;


public interface DepartmentService {

    List<DepartmentDto> getAllDepartments();

    DepartmentDto getDepartmentById(Long id);

    void createNewDepartment(DepartmentDto departmentDto);

    void updateDepartment(DepartmentDto departmentDto, Long id);

    void deleteDepartment(Long id);

}
