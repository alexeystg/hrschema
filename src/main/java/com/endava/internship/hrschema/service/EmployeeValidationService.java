package com.endava.internship.hrschema.service;


import com.endava.internship.hrschema.model.request.EmployeeCreateRequest;
import com.endava.internship.hrschema.model.request.EmployeeUpdateRequest;

public interface EmployeeValidationService {

    void validateEmployee(EmployeeCreateRequest request);

    void validateEmployee(EmployeeUpdateRequest request);
}
