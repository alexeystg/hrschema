package com.endava.internship.hrschema.utils.employee;


import com.endava.internship.hrschema.model.dto.EmployeeDto;
import com.endava.internship.hrschema.model.entity.DepartmentEntity;
import com.endava.internship.hrschema.model.entity.EmployeeEntity;
import com.endava.internship.hrschema.model.request.EmployeeCreateRequest;
import com.endava.internship.hrschema.model.request.EmployeeUpdateRequest;
import com.endava.internship.hrschema.model.response.EmployeeExpandedResponse;
import com.endava.internship.hrschema.model.response.EmployeeResponse;

import static java.util.Objects.nonNull;

public class EmployeeConverter {

    private EmployeeConverter() {
    }

    public static EmployeeDto convertFromEntityToDto(EmployeeEntity employeeEntity) {
        EmployeeDto employeeDto = EmployeeDto.builder()
                .id(employeeEntity.getId())
                .firstName(employeeEntity.getFirstName())
                .lastName(employeeEntity.getLastName())
                .commissionPct(employeeEntity.getCommissionPct())
                .email(employeeEntity.getEmail())
                .job(employeeEntity.getJob())
                .hireDate(employeeEntity.getHireDate())
                .phoneNumber(employeeEntity.getPhoneNumber())
                .salary(employeeEntity.getSalary())
                .build();
        DepartmentEntity department = employeeEntity.getDepartmentId();
        employeeDto.setDepartmentId(department == null ? null : department.getId());
        EmployeeEntity manager = employeeEntity.getManagerId();
        employeeDto.setManagerId(manager == null ? null : manager.getId());
        return employeeDto;
    }

    public static EmployeeExpandedResponse convertFromDtoToExpandedResponse(EmployeeDto employeeDto) {
        return new EmployeeExpandedResponse(
                employeeDto.getFirstName(),
                employeeDto.getLastName(),
                employeeDto.getEmail(),
                employeeDto.getPhoneNumber(),
                employeeDto.getHireDate(),
                employeeDto.getJob(),
                employeeDto.getSalary(),
                employeeDto.getCommissionPct(),
                employeeDto.getManagerId(),
                employeeDto.getDepartmentId()
        );
    }

    public static EmployeeDto convertFromCreateRequestToDto(EmployeeCreateRequest employee) {
        return EmployeeDto.builder()
                .firstName(employee.getFirstName())
                .lastName(employee.getLastName())
                .commissionPct(employee.getCommissionPct())
                .departmentId(employee.getDepartmentId())
                .email(employee.getEmail())
                .job(employee.getJob())
                .hireDate(employee.getHireDate())
                .managerId(employee.getManagerId())
                .phoneNumber(employee.getPhoneNumber())
                .salary(employee.getSalary())
                .build();
    }

    public static EmployeeDto convertFromUpdateRequestToDto(EmployeeUpdateRequest employee) {
        return EmployeeDto.builder()
                .firstName(employee.getFirstName())
                .lastName(employee.getLastName())
                .commissionPct(employee.getCommissionPct())
                .departmentId(employee.getDepartmentId())
                .email(employee.getEmail())
                .job(employee.getJob())
                .hireDate(employee.getHireDate())
                .managerId(employee.getManagerId())
                .phoneNumber(employee.getPhoneNumber())
                .salary(employee.getSalary())
                .build();
    }

    public static EmployeeEntity convertFromDtoToEntity(EmployeeDto employeeDto) {
        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setId(employeeDto.getId());
        employeeEntity.setEmail(employeeDto.getEmail());

        EmployeeEntity manager = null;
        if (nonNull(employeeDto.getManagerId())) {
            manager = new EmployeeEntity();
            manager.setId(employeeDto.getManagerId());
        }
        employeeEntity.setManagerId(manager);

        employeeEntity.setCommissionPct(employeeDto.getCommissionPct());

        DepartmentEntity department = null;
        if (nonNull(employeeDto.getDepartmentId())) {
            department = new DepartmentEntity();
            department.setId(employeeDto.getDepartmentId());
        }
        employeeEntity.setDepartmentId(department);

        employeeEntity.setFirstName(employeeDto.getFirstName());
        employeeEntity.setLastName(employeeDto.getLastName());
        employeeEntity.setHireDate(employeeDto.getHireDate());
        employeeEntity.setPhoneNumber(employeeDto.getPhoneNumber());
        employeeEntity.setJob(employeeDto.getJob());
        employeeEntity.setSalary(employeeDto.getSalary());
        return employeeEntity;

    }

    public static EmployeeResponse convertFromDtoToResponse(EmployeeDto employeeDto) {
        return new EmployeeResponse(
                employeeDto.getFirstName(),
                employeeDto.getLastName(),
                employeeDto.getDepartmentId()
        );
    }
}
