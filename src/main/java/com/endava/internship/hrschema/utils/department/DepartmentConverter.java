package com.endava.internship.hrschema.utils.department;

import com.endava.internship.hrschema.model.dto.DepartmentDto;
import com.endava.internship.hrschema.model.entity.DepartmentEntity;
import com.endava.internship.hrschema.model.entity.EmployeeEntity;
import com.endava.internship.hrschema.model.request.DepartmentCreateRequest;
import com.endava.internship.hrschema.model.request.DepartmentUpdateRequest;
import com.endava.internship.hrschema.model.response.DepartmentExpandedResponse;
import com.endava.internship.hrschema.model.response.DepartmentResponse;

import static java.util.Objects.nonNull;


public class DepartmentConverter {

    private DepartmentConverter() {
    }

    public static DepartmentExpandedResponse convertFromDtoToExpandedResponse(DepartmentDto departmentDto) {
        return new DepartmentExpandedResponse(
                departmentDto.getDepartmentName(),
                departmentDto.getManagerId(),
                departmentDto.getLocationId()
        );
    }

    public static DepartmentDto convertFromEntityToDto(DepartmentEntity departmentEntity) {
        DepartmentDto departmentDto = DepartmentDto.builder()
                .departmentId(departmentEntity.getId())
                .departmentName(departmentEntity.getDepartmentName())
                .locationId(departmentEntity.getLocationId())
                .build();
        EmployeeEntity manager = departmentEntity.getManagerId();
        departmentDto.setManagerId(manager == null ? null : manager.getId());
        return departmentDto;
    }

    public static DepartmentDto convertFromCreateRequestToDto(DepartmentCreateRequest department) {
        return DepartmentDto.builder()
                .departmentName(department.getDepartmentName())
                .managerId(department.getManagerId())
                .locationId(department.getLocationId())
                .build();
    }

    public static DepartmentEntity convertFromDtoToEntity(DepartmentDto departmentDto) {
        DepartmentEntity converted = new DepartmentEntity();
        converted.setId(departmentDto.getDepartmentId());
        converted.setDepartmentName(departmentDto.getDepartmentName());
        converted.setLocationId(departmentDto.getLocationId());

        EmployeeEntity manager = null;
        if (nonNull(departmentDto.getManagerId())) {
            manager = new EmployeeEntity();
            manager.setId(departmentDto.getManagerId());
        }
        converted.setManagerId(manager);

        return converted;
    }

    public static DepartmentDto convertFromUpdateRequestToDto(DepartmentUpdateRequest department) {
        return DepartmentDto.builder()
                .departmentName(department.getDepartmentName())
                .managerId(department.getManagerId())
                .locationId(department.getLocationId())
                .build();
    }

    public static DepartmentResponse convertFromDtoToResponse(DepartmentDto departmentDto) {
        return new DepartmentResponse(departmentDto.getDepartmentName());
    }
}
