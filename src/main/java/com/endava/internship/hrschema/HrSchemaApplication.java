package com.endava.internship.hrschema;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class HrSchemaApplication {

	public static void main(String[] args) {
		SpringApplication.run(HrSchemaApplication.class, args);
	}

}
