package utils;

public class EmployeeTestConstants {
    public static String FIRST_NAME = "John";
    public static String LAST_NAME = "Doe";
    public static Long DEPARTMENT_ID = 10L;
    public static double SALARY = 5000.0;
    public static String EMAIL = "doe@gmail.com";
    public static String PHONE_NUMBER = "012345678";
    public static String JOB = "ST_MAN";

    public static String FIRST_NAME2 = "Mary";
    public static String LAST_NAME2 = "Smith";
    public static Long DEPARTMENT_ID2 = 20L;
    public static double SALARY2 = 4000.0;
    public static String EMAIL2 = "smith@gmail.com";
    public static Float COMMISSION_PCT = 0.2f;
}
