package com.endava.internship.hrschema.facade;

import com.endava.internship.hrschema.model.dto.DepartmentDto;
import com.endava.internship.hrschema.service.DepartmentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DepartmentFacadeTest {

    @Mock
    DepartmentService service;

    @InjectMocks
    DepartmentFacade facade;

    @Test
    void getAllDepartments() {
        //Given
        List<DepartmentDto> expectedDtos = getDepartmentDtos();
        when(service.getAllDepartments()).thenReturn(expectedDtos);

        //When
        List<DepartmentDto> actualDtos = facade.getAllDepartments();

        //Then
        assertThat(actualDtos).containsExactlyInAnyOrderElementsOf(expectedDtos);

        verify(service).getAllDepartments();
    }

    @Test
    void getDepartmentById() {
        //Given
        Long id = 1L;
        DepartmentDto expectedDepartment = DepartmentDto.builder()
                .departmentId(1L)
                .departmentName("Test")
                .build();
        when(service.getDepartmentById(id)).thenReturn(expectedDepartment);

        //When
        DepartmentDto actualDepartment = facade.getDepartmentById(1L);

        //Then
        assertThat(actualDepartment).isEqualTo(expectedDepartment);

        verify(service).getDepartmentById(id);
    }

    @Test
    void createNewDepartment() {
        //Given
        DepartmentDto departmentDto = DepartmentDto.builder()
                .departmentId(5L)
                .departmentName("Test")
                .build();

        //When
        facade.createDepartment(departmentDto);

        //Then
        verify(service).createNewDepartment(departmentDto);
    }

    @Test
    void updateDepartment() {
        //Given
        DepartmentDto departmentDto = DepartmentDto.builder()
                .departmentId(3L)
                .departmentName("test")
                .build();
        Long id = 2L;

        //When
        facade.updateDepartment(departmentDto, id);

        //Then
        verify(service).updateDepartment(departmentDto, id);
    }

    @Test
    void deleteDepartment() {
        //Given
        Long id = 10L;

        //When
        facade.deleteDepartment(id);

        //Then
        verify(service).deleteDepartment(id);
    }

    private List<DepartmentDto> getDepartmentDtos() {
        List<DepartmentDto> departmentDtos = new ArrayList<>();
        departmentDtos.add(DepartmentDto.builder()
                .departmentName("Test")
                .locationId(30L)
                .managerId(10L)
                .build());
        departmentDtos.add(DepartmentDto.builder()
                .departmentName("Test2")
                .locationId(40L)
                .managerId(20L)
                .build());
        return departmentDtos;
    }
}