package com.endava.internship.hrschema.facade;

import com.endava.internship.hrschema.model.dto.EmployeeDto;
import com.endava.internship.hrschema.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmployeeFacadeTest {

    @Mock
    EmployeeService service;

    @InjectMocks
    EmployeeFacade facade;

    @Test
    void getAllEmployees() {
        //Given
        List<EmployeeDto> expectedDtos = getEmployeeDtos();
        when(service.getAllEmployees()).thenReturn(expectedDtos);

        //When
        List<EmployeeDto> actualDtos = facade.getAllEmployees();

        //Then
        assertThat(actualDtos).containsExactlyInAnyOrderElementsOf(expectedDtos);

        verify(service).getAllEmployees();
    }

    @Test
    void getEmployeeById() {
        //Given
        Long id = 1L;
        EmployeeDto expectedEmployee = EmployeeDto.builder()
                .id(1L)
                .firstName("John")
                .build();
        when(service.getEmployeeById(id)).thenReturn(expectedEmployee);

        //When
        EmployeeDto actualEmployee = facade.getEmployeeById(1L);

        //Then
        assertThat(actualEmployee).isEqualTo(expectedEmployee);

        verify(service).getEmployeeById(id);
    }

    @Test
    void createNewEmployee() {
        //Given
        EmployeeDto employeeDto = EmployeeDto.builder()
                .id(5L)
                .firstName("John")
                .lastName("Doe")
                .build();

        //When
        facade.createNewEmployee(employeeDto);

        //Then
        verify(service).createNewEmployee(employeeDto);
    }

    @Test
    void updateEmployee() {
        //Given
        EmployeeDto employeeDto = EmployeeDto.builder()
                .id(5L)
                .firstName("John")
                .lastName("Doe")
                .build();
        Long id = 4L;

        //When
        facade.updateEmployee(employeeDto, id);

        //Then
        verify(service).updateEmployee(employeeDto, id);
    }

    @Test
    void deleteEmployee() {
        //Given
        Long id = 3L;

        //When
        facade.deleteEmployee(id);

        //Then
        verify(service).deleteEmployee(id);
    }

    private List<EmployeeDto> getEmployeeDtos() {
        List<EmployeeDto> employeeDtos = new ArrayList<>();
        employeeDtos.add(EmployeeDto.builder().
                firstName("John").
                lastName("Doe")
                .departmentId(10L)
                .salary(5000.0)
                .email("test@mail.ru")
                .build());
        employeeDtos.add(EmployeeDto.builder()
                .firstName("Mary")
                .lastName("Smith")
                .salary(2500.0)
                .departmentId(20L)
                .commissionPct(0.2f)
                .build());
        return employeeDtos;
    }
}