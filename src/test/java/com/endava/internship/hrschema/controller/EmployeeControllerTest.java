package com.endava.internship.hrschema.controller;

import com.endava.internship.hrschema.facade.EmployeeFacade;
import com.endava.internship.hrschema.model.dto.EmployeeDto;
import com.endava.internship.hrschema.model.request.EmployeeCreateRequest;
import com.endava.internship.hrschema.model.request.EmployeeUpdateRequest;
import com.endava.internship.hrschema.model.response.EmployeeExpandedResponse;
import com.endava.internship.hrschema.model.response.EmployeeResponse;
import com.endava.internship.hrschema.service.EmployeeValidationService;
import com.endava.internship.hrschema.utils.employee.EmployeeConverter;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import utils.EmployeeTestConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static utils.EmployeeTestConstants.*;

@ExtendWith(MockitoExtension.class)
class EmployeeControllerTest {

    @Mock
    EmployeeFacade facade;

    @Mock
    EmployeeValidationService employeeValidationService;

    @InjectMocks
    EmployeeController controller;


    @Test
    void getAllEmployees() {
        //Given
        List<EmployeeDto> employeesDtos = getEmployeeDtos();
        List<EmployeeResponse> expectedEmployees = getResponses();
        when(facade.getAllEmployees()).thenReturn(employeesDtos);

        //When
        ResponseEntity<List<Object>> actualEmployees = controller.getAllEmployees(false);

        //Then
        assertThat(actualEmployees.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualEmployees.getBody()).containsExactlyInAnyOrderElementsOf(expectedEmployees);

        verify(facade).getAllEmployees();
    }

    @Test
    void getAllEmployeesWithExpandTrue() {
        //Given
        List<EmployeeDto> employeesDtos = getEmployeeDtos();
        List<EmployeeExpandedResponse> expectedResponse = getExpandedResponses();
        when(facade.getAllEmployees()).thenReturn(employeesDtos);


        //When
        ResponseEntity<List<Object>> actualEmployees = controller.getAllEmployees(true);

        //Then
        assertThat(actualEmployees.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualEmployees.getBody()).containsExactlyInAnyOrderElementsOf(expectedResponse);

        verify(facade).getAllEmployees();
    }


    @Test
    void getEmployeeById() {
        //Given
        EmployeeDto employeeDto = getEmployeeDtos().get(0);
        EmployeeResponse expectedResponse = getResponses().get(0);
        when(facade.getEmployeeById(2L)).thenReturn(employeeDto);

        //When
        ResponseEntity<?> actualResponse = controller.getEmployeeById(2L, false);

        //Then
        assertThat(actualResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualResponse.getBody()).isEqualTo(expectedResponse);

        verify(facade).getEmployeeById(2L);
    }

    @Test
    void getEmployeeByIdWithExpandTrue() {
        //Given
        EmployeeDto employeeDto = getEmployeeDtos().get(0);
        EmployeeExpandedResponse expectedResponse = getExpandedResponses().get(0);
        when(facade.getEmployeeById(2L)).thenReturn(employeeDto);

        //When
        ResponseEntity<?> actualResponse = controller.getEmployeeById(2L, true);

        //Then
        assertThat(actualResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualResponse.getBody()).isEqualTo(expectedResponse);

        verify(facade).getEmployeeById(2L);
    }

    @Test
    void addNewEmployee() {
        //Given
        EmployeeCreateRequest employee = EmployeeCreateRequest.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .phoneNumber(PHONE_NUMBER)
                .job(JOB)
                .salary(SALARY)
                .build();

        //When
        controller.addNewEmployee(employee);

        //Then
        verify(facade).createNewEmployee(EmployeeConverter.convertFromCreateRequestToDto(employee));
        verify(employeeValidationService).validateEmployee(employee);
    }

    @Test
    void updateEmployee() {
        //Given
        EmployeeUpdateRequest employee = EmployeeUpdateRequest.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .phoneNumber(PHONE_NUMBER)
                .job(JOB)
                .salary(SALARY)
                .build();
        Long id = 1L;

        //When
        controller.updateEmployee(employee, id);

        //Then
        verify(facade).updateEmployee(EmployeeConverter.convertFromUpdateRequestToDto(employee), id);
        verify(employeeValidationService).validateEmployee(employee);
    }

    @Test
    void deleteEmployeeById() {
        //Given
        Long id = 1L;

        //When
        controller.deleteEmployeeById(id);

        //Then
        verify(facade).deleteEmployee(id);
    }

    private List<EmployeeExpandedResponse> getExpandedResponses() {
        List<EmployeeExpandedResponse> employeeExpandedResponses = new ArrayList<>();
        EmployeeExpandedResponse firstEmployee = new EmployeeExpandedResponse();
        firstEmployee.setFirstName(FIRST_NAME);
        firstEmployee.setLastName(LAST_NAME);
        firstEmployee.setDepartmentId(DEPARTMENT_ID);
        firstEmployee.setSalary(SALARY);
        firstEmployee.setEmail(EMAIL);

        EmployeeExpandedResponse secondEmployee = new EmployeeExpandedResponse();
        secondEmployee.setFirstName(FIRST_NAME2);
        secondEmployee.setLastName(LAST_NAME2);
        secondEmployee.setSalary(SALARY2);
        secondEmployee.setDepartmentId(DEPARTMENT_ID2);
        secondEmployee.setCommissionPct(COMMISSION_PCT);
        secondEmployee.setEmail(EMAIL2);

        employeeExpandedResponses.add(firstEmployee);
        employeeExpandedResponses.add(secondEmployee);
        return employeeExpandedResponses;
    }

    private List<EmployeeResponse> getResponses() {
        List<EmployeeResponse> employeeResponses = new ArrayList<>();
        EmployeeResponse firstEmployee = new EmployeeResponse();
        firstEmployee.setFirstName(FIRST_NAME);
        firstEmployee.setLastName(LAST_NAME);
        firstEmployee.setDepartmentId(DEPARTMENT_ID);

        EmployeeResponse secondEmployee = new EmployeeResponse();
        secondEmployee.setFirstName(FIRST_NAME2);
        secondEmployee.setLastName(LAST_NAME2);
        secondEmployee.setDepartmentId(DEPARTMENT_ID2);

        employeeResponses.add(firstEmployee);
        employeeResponses.add(secondEmployee);
        return employeeResponses;
    }

    private List<EmployeeDto> getEmployeeDtos() {
        List<EmployeeDto> employeeDtos = new ArrayList<>();
        employeeDtos.add(EmployeeDto.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .departmentId(DEPARTMENT_ID)
                .salary(SALARY)
                .email(EMAIL)
                .build());
        employeeDtos.add(EmployeeDto.builder()
                .firstName(FIRST_NAME2)
                .lastName(LAST_NAME2)
                .salary(SALARY2)
                .departmentId(DEPARTMENT_ID2)
                .email(EMAIL2)
                .commissionPct(COMMISSION_PCT)
                .build());
        return employeeDtos;
    }
}