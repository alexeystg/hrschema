package com.endava.internship.hrschema.controller;

import com.endava.internship.hrschema.facade.DepartmentFacade;
import com.endava.internship.hrschema.model.dto.DepartmentDto;
import com.endava.internship.hrschema.model.request.DepartmentCreateRequest;
import com.endava.internship.hrschema.model.request.DepartmentUpdateRequest;
import com.endava.internship.hrschema.model.response.DepartmentExpandedResponse;
import com.endava.internship.hrschema.model.response.DepartmentResponse;
import com.endava.internship.hrschema.service.DepartmentValidationService;
import com.endava.internship.hrschema.utils.department.DepartmentConverter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import utils.DepartmentTestConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static utils.DepartmentTestConstants.*;

@ExtendWith(MockitoExtension.class)
class DepartmentControllerTest {

    @Mock
    DepartmentFacade facade;

    @Mock
    DepartmentValidationService departmentValidationService;

    @InjectMocks
    DepartmentController controller;

    @Test
    void getAllDepartments() {
        //Given
        List<DepartmentDto> departmentsDtos = getDepartmentDtos();
        List<DepartmentResponse> expectedDepartments = getResponses();
        when(facade.getAllDepartments()).thenReturn(departmentsDtos);

        //When
        ResponseEntity<List<Object>> actualDepartments = controller.getAllDepartments(false);

        //Then
        assertThat(actualDepartments.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualDepartments.getBody()).containsExactlyInAnyOrderElementsOf(expectedDepartments);

        verify(facade).getAllDepartments();
    }

    @Test
    void getAllDepartmentsWithExpandTrue() {
        //Given
        List<DepartmentDto> departmentsDtos = getDepartmentDtos();
        List<DepartmentExpandedResponse> expectedResponse = getExpandedResponses();
        when(facade.getAllDepartments()).thenReturn(departmentsDtos);


        //When
        ResponseEntity<List<Object>> actualDepartments = controller.getAllDepartments(true);

        //Then
        assertThat(actualDepartments.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualDepartments.getBody()).containsExactlyInAnyOrderElementsOf(expectedResponse);

        verify(facade).getAllDepartments();
    }


    @Test
    void getDepartmentById() {
        //Given
        DepartmentDto departmentDto = getDepartmentDtos().get(0);
        DepartmentResponse expectedResponse = getResponses().get(0);
        when(facade.getDepartmentById(2L)).thenReturn(departmentDto);

        //When
        ResponseEntity<Object> actualResponse = controller.getDepartmentById(2L, false);

        //Then
        assertThat(actualResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualResponse.getBody()).isEqualTo(expectedResponse);

        verify(facade).getDepartmentById(2L);
    }

    @Test
    void getDepartmentByIdWithExpandTrue() {
        //Given
        DepartmentDto departmentDto = getDepartmentDtos().get(0);
        DepartmentExpandedResponse expectedResponse = getExpandedResponses().get(0);
        when(facade.getDepartmentById(2L)).thenReturn(departmentDto);

        //When
        ResponseEntity<Object> actualResponse = controller.getDepartmentById(2L, true);

        //Then
        assertThat(actualResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualResponse.getBody()).isEqualTo(expectedResponse);

        verify(facade).getDepartmentById(2L);
    }

    @Test
    void addNewDepartment() {
        //Given
        DepartmentCreateRequest department = DepartmentCreateRequest.builder()
                .departmentName(DEPARTMENT_NAME)
                .locationId(LOCATION_ID)
                .build();

        //When
        controller.createDepartment(department);

        //Then
        verify(facade).createDepartment(DepartmentConverter.convertFromCreateRequestToDto(department));
        verify(departmentValidationService).validateDepartment(department);
    }

    @Test
    void updateDepartment() {
        //Given
        DepartmentUpdateRequest department = DepartmentUpdateRequest.builder()
                .departmentName(DEPARTMENT_NAME)
                .locationId(LOCATION_ID)
                .build();
        Long id = 1L;

        //When
        controller.updateDepartment(department, id);

        //Then
        verify(facade).updateDepartment(DepartmentConverter.convertFromUpdateRequestToDto(department), id);
        verify(departmentValidationService).validateDepartment(department);
    }

    @Test
    void deleteDepartmentById() {
        //Given
        Long id = 1L;

        //When
        controller.deleteDepartmentById(id);

        //Then
        verify(facade).deleteDepartment(id);
    }

    private List<DepartmentExpandedResponse> getExpandedResponses() {
        return Arrays.asList(new DepartmentExpandedResponse(DEPARTMENT_NAME, LOCATION_ID, MANAGER_ID),
                new DepartmentExpandedResponse(DEPARTMENT_NAME2, LOCATION_ID2, MANAGER_ID2)
        );
    }

    private List<DepartmentResponse> getResponses() {
        return Arrays.asList(new DepartmentResponse(DEPARTMENT_NAME),
                new DepartmentResponse(DEPARTMENT_NAME2)
        );
    }

    private List<DepartmentDto> getDepartmentDtos() {
        List<DepartmentDto> departmentDtos = new ArrayList<>();
        departmentDtos.add(DepartmentDto.builder()
                .departmentName(DEPARTMENT_NAME)
                .locationId(LOCATION_ID)
                .managerId(MANAGER_ID)
                .build());
        departmentDtos.add(DepartmentDto.builder()
                .departmentName(DEPARTMENT_NAME2)
                .locationId(LOCATION_ID2)
                .managerId(MANAGER_ID2)
                .build());
        return departmentDtos;
    }
}