package com.endava.internship.hrschema.utils.employee;

import com.endava.internship.hrschema.model.dto.EmployeeDto;
import com.endava.internship.hrschema.model.entity.DepartmentEntity;
import com.endava.internship.hrschema.model.entity.EmployeeEntity;
import com.endava.internship.hrschema.model.request.EmployeeCreateRequest;
import com.endava.internship.hrschema.model.request.EmployeeUpdateRequest;
import com.endava.internship.hrschema.model.response.EmployeeExpandedResponse;
import com.endava.internship.hrschema.model.response.EmployeeResponse;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;


class EmployeeConverterTest {

    @Test
    void convertFromEntityToDto() {
        //Given
        EmployeeEntity employeeEntity = getEmployeeEntity();
        EmployeeDto expectedDto = getEmployeeDto();

        //When
        EmployeeDto actualDto = EmployeeConverter.convertFromEntityToDto(employeeEntity);

        //Then
        assertThat(actualDto).isEqualTo(expectedDto);
    }

    @Test
    void convertFromDtoToExpandedResponse() {
        //Given
        EmployeeDto employeeDto = getEmployeeDto();
        EmployeeExpandedResponse expectedResponse = new EmployeeExpandedResponse(
                "John",
                "Doe",
                null,
                null,
                null,
                "Test",
                null,
                null,
                null,
                20L);

        //When
        EmployeeExpandedResponse actualResponse = EmployeeConverter.convertFromDtoToExpandedResponse(employeeDto);

        //Then
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    void convertFromCreateRequestToDto() {
        //Given
        EmployeeCreateRequest employeeCreateRequest = EmployeeCreateRequest.builder()
                .job("Test")
                .firstName("John")
                .lastName("Doe")
                .departmentId(20L)
                .build();
        EmployeeDto expectedDto = EmployeeDto.builder()
                .job("Test")
                .firstName("John")
                .lastName("Doe")
                .departmentId(20L)
                .build();

        //When
        EmployeeDto actualDto = EmployeeConverter.convertFromCreateRequestToDto(employeeCreateRequest);

        //Then
        assertThat(actualDto).isEqualTo(expectedDto);
    }

    @Test
    void convertFromUpdateRequestToDto() {
        //Given
        EmployeeUpdateRequest employeeUpdateRequest = EmployeeUpdateRequest.builder()
                .job("Test")
                .firstName("John")
                .lastName("Doe")
                .departmentId(20L)
                .build();
        EmployeeDto expectedDto = EmployeeDto.builder()
                .job("Test")
                .firstName("John")
                .lastName("Doe")
                .departmentId(20L)
                .build();

        //When
        EmployeeDto actualDto = EmployeeConverter.convertFromUpdateRequestToDto(employeeUpdateRequest);

        //Then
        assertThat(actualDto).isEqualTo(expectedDto);
    }

    @Test
    void convertFromDtoToEntity() {
        //Given
        EmployeeEntity expectedEntity = getEmployeeEntity();
        EmployeeDto employeeDto = getEmployeeDto();

        //When
        EmployeeEntity actualEntity = EmployeeConverter.convertFromDtoToEntity(employeeDto);

        //Then
        assertThat(actualEntity).isEqualTo(expectedEntity);
    }

    @Test
    void convertFromDtoToResponse() {
        //Given
        EmployeeDto employeeDto = getEmployeeDto();
        EmployeeResponse expectedResponse = new EmployeeResponse("John", "Doe", 20L);

        //When
        EmployeeResponse actualResponse = EmployeeConverter.convertFromDtoToResponse(employeeDto);

        //Then
        assertThat(actualResponse).isEqualTo(expectedResponse);

    }

    private EmployeeDto getEmployeeDto() {
        return EmployeeDto.builder()
                .id(1L)
                .job("Test")
                .firstName("John")
                .lastName("Doe")
                .departmentId(20L)
                .build();
    }

    private EmployeeEntity getEmployeeEntity() {
        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setId(1L);
        employeeEntity.setJob("Test");
        employeeEntity.setFirstName("John");
        employeeEntity.setLastName("Doe");
        DepartmentEntity departmentEntity = new DepartmentEntity();
        departmentEntity.setId(20L);
        employeeEntity.setDepartmentId(departmentEntity);
        return employeeEntity;
    }
}