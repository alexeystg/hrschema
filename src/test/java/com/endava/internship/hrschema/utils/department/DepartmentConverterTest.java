package com.endava.internship.hrschema.utils.department;

import com.endava.internship.hrschema.model.dto.DepartmentDto;
import com.endava.internship.hrschema.model.entity.DepartmentEntity;
import com.endava.internship.hrschema.model.request.DepartmentCreateRequest;
import com.endava.internship.hrschema.model.request.DepartmentUpdateRequest;
import com.endava.internship.hrschema.model.response.DepartmentResponse;
import com.endava.internship.hrschema.model.response.DepartmentExpandedResponse;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

class DepartmentConverterTest {


    @Test
    void convertFromDtoToResponse() {
        //Given
        DepartmentDto departmentDto = DepartmentDto.builder()
                .departmentName("Test")
                .departmentId(20L)
                .managerId(10L).build();
        DepartmentResponse expectedResponse = new DepartmentResponse("Test");

        //When
        DepartmentResponse actualResponse = DepartmentConverter.convertFromDtoToResponse(departmentDto);

        //Then
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    void convertFromEntityToDto() {
        //Given
        DepartmentEntity departmentEntity = new DepartmentEntity();
        departmentEntity.setId(2L);
        departmentEntity.setDepartmentName("Test");
        departmentEntity.setLocationId(10L);
        DepartmentDto expectedDto = DepartmentDto.builder()
                .departmentName("Test")
                .departmentId(2L)
                .locationId(10L)
                .build();

        //When
        DepartmentDto actualDto = DepartmentConverter.convertFromEntityToDto(departmentEntity);

        //Then
        assertThat(actualDto).isEqualTo(expectedDto);
    }

    @Test
    void convertFromCreateRequestToDto() {
        //Given
        DepartmentCreateRequest departmentDto = DepartmentCreateRequest.builder()
                .departmentName("John")
                .managerId(10L)
                .build();
        DepartmentDto expectedDto = DepartmentDto.builder()
                .departmentName("John")
                .managerId(10L)
                .build();

        //When
        DepartmentDto actualDto = DepartmentConverter.convertFromCreateRequestToDto(departmentDto);

        //Then
        assertThat(actualDto).isEqualTo(expectedDto);
    }

    @Test
    void convertFromDtoToEntity() {
        //Given
        DepartmentDto departmentDto = DepartmentDto.builder()
                .departmentName("John")
                .departmentId(20L)
                .managerId(null)
                .build();
        DepartmentEntity expectedEntity = new DepartmentEntity();
        expectedEntity.setDepartmentName("John");
        expectedEntity.setId(20L);
        expectedEntity.setManagerId(null);

        //When
        DepartmentEntity actualEntity = DepartmentConverter.convertFromDtoToEntity(departmentDto);

        //Then
        assertThat(actualEntity).isEqualTo(expectedEntity);
    }

    @Test
    void convertFromUpdateRequestToDto() {
        //Given
        DepartmentUpdateRequest departmentDto = DepartmentUpdateRequest.builder()
                .departmentName("John")
                .managerId(10L)
                .build();
        DepartmentDto expectedDto = DepartmentDto.builder()
                .departmentName("John")
                .managerId(10L)
                .build();

        //When
        DepartmentDto actualDto = DepartmentConverter.convertFromUpdateRequestToDto(departmentDto);

        //Then
        assertThat(actualDto).isEqualTo(expectedDto);
    }

    @Test
    void convertFromDtoToExpandedResponse() {
        //Given
        DepartmentDto departmentDto = DepartmentDto.builder()
                .departmentName("Test")
                .locationId(20L)
                .managerId(10L)
                .build();
        DepartmentExpandedResponse expectedExpandedResponse = new DepartmentExpandedResponse("Test", 10L, 20L);

        //When
        DepartmentExpandedResponse actualExpandedResponse = DepartmentConverter
                .convertFromDtoToExpandedResponse(departmentDto);

        //Then
        assertThat(actualExpandedResponse).isEqualTo(expectedExpandedResponse);
    }
}