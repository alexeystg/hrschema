package com.endava.internship.hrschema.service;

import com.endava.internship.hrschema.model.dto.DepartmentDto;
import com.endava.internship.hrschema.model.entity.DepartmentEntity;
import com.endava.internship.hrschema.repository.DepartmentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DepartmentServiceImplTest {

    @Mock
    DepartmentRepository repository;

    @InjectMocks
    DepartmentServiceImpl service;

    @Test
    void getAllDepartments() {
        //Given
        List<DepartmentDto> expectedDtos = getDepartmentDtos();
        List<DepartmentEntity> departmentEntities = getDepartmentEntities();
        when(repository.findAll()).thenReturn(departmentEntities);

        //When
        List<DepartmentDto> actualDtos = service.getAllDepartments();

        //Then
        assertThat(actualDtos).containsExactlyInAnyOrderElementsOf(expectedDtos);

        verify(repository).findAll();
    }

    @Test
    void getDepartmentById() {
        //Given
        DepartmentDto expectedDto = getDepartmentDtos().get(0);
        DepartmentEntity departmentEntity = getDepartmentEntities().get(0);
        when(repository.findById(2L)).thenReturn(Optional.of(departmentEntity));

        //When
        DepartmentDto actualDto = service.getDepartmentById(2L);

        //Then
        assertThat(actualDto).isEqualTo(expectedDto);

        verify(repository).findById(2L);
    }

    @Test
    void createNewDepartment() {
        //Given
        DepartmentDto departmentDto = getDepartmentDtos().get(0);
        DepartmentEntity departmentEntity = getDepartmentEntities().get(0);

        //When
        service.createNewDepartment(departmentDto);

        //Then
        verify(repository).save(departmentEntity);
    }

    @Test
    void updateDepartment() {
        //Given
        DepartmentDto departmentDto = getDepartmentDtos().get(0);
        DepartmentEntity departmentEntity = getDepartmentEntities().get(0);

        //When
        service.updateDepartment(departmentDto, 2L);

        //Then
        verify(repository).save(departmentEntity);
    }

    @Test
    void deleteDepartment() {
        //Given
        Long id = 2L;

        //When
        service.deleteDepartment(id);

        //Then
        verify(repository).deleteById(id);
    }

    private static List<DepartmentDto> getDepartmentDtos() {
        List<DepartmentDto> departmentDtos = new ArrayList<>();
        departmentDtos.add(DepartmentDto.builder()
                .departmentId(2L)
                .departmentName("Test")
                .locationId(10L)
                .managerId(null)
                .build());
        departmentDtos.add(DepartmentDto.builder()
                .departmentId(4L)
                .departmentName("Test2")
                .locationId(20L)
                .managerId(null)
                .build());
        return departmentDtos;
    }

    private static List<DepartmentEntity> getDepartmentEntities() {
        List<DepartmentEntity> departmentEntities = new ArrayList<>();
        DepartmentEntity entity1 = new DepartmentEntity();
        entity1.setId(2L);
        entity1.setDepartmentName("Test");
        entity1.setLocationId(10L);
        entity1.setManagerId(null);
        DepartmentEntity entity2 = new DepartmentEntity();
        entity2.setId(4L);
        entity2.setDepartmentName("Test2");
        entity2.setLocationId(20L);
        entity2.setManagerId(null);
        departmentEntities.add(entity1);
        departmentEntities.add(entity2);
        return departmentEntities;
    }
}