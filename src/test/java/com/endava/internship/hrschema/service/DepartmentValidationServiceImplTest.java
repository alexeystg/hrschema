package com.endava.internship.hrschema.service;

import com.endava.internship.hrschema.exception.HrSchemaException;
import com.endava.internship.hrschema.model.request.DepartmentCreateRequest;
import com.endava.internship.hrschema.model.request.DepartmentUpdateRequest;
import org.junit.jupiter.api.Test;

import static com.endava.internship.hrschema.exception.ExceptionType.INVALID_DEPARTMENT_NAME;
import static com.endava.internship.hrschema.exception.ExceptionType.INVALID_LOCATION_ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static utils.DepartmentTestConstants.*;

class DepartmentValidationServiceImplTest {

    DepartmentValidationService validationService = new DepartmentValidationServiceImpl();

    @Test
    void validateEmployeeShouldThrowExceptionContainingInvalidNameErrorOnCreate() {
        //Given
        DepartmentCreateRequest request = DepartmentCreateRequest.builder()
                .departmentName(null)
                .locationId(LOCATION_ID)
                .managerId(MANAGER_ID)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateDepartment(request),
                HrSchemaException.class);

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_DEPARTMENT_NAME);
    }

    @Test
    void validateEmployeeShouldThrowExceptionContainingLocationIdErrorOnCreate() {
        //Given
        DepartmentCreateRequest request = DepartmentCreateRequest.builder()
                .departmentName(DEPARTMENT_NAME)
                .locationId(null)
                .managerId(MANAGER_ID)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateDepartment(request),
                HrSchemaException.class);

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_LOCATION_ID);
    }

    @Test
    void validateEmployeeShouldNotThrowExceptionOnCreate() {
        //Given
        DepartmentCreateRequest request = DepartmentCreateRequest.builder()
                .departmentName(DEPARTMENT_NAME)
                .locationId(LOCATION_ID)
                .managerId(MANAGER_ID)
                .build();

        //When
        validationService.validateDepartment(request);
    }

    @Test
    void validateEmployeeShouldThrowExceptionContainingInvalidNameErrorOnUpdate() {
        //Given
        DepartmentUpdateRequest request = DepartmentUpdateRequest.builder()
                .departmentName(null)
                .locationId(LOCATION_ID)
                .managerId(MANAGER_ID)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateDepartment(request),
                HrSchemaException.class);

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_DEPARTMENT_NAME);
    }

    @Test
    void validateEmployeeShouldThrowExceptionContainingLocationIdErrorOnUpdate() {
        //Given
        DepartmentUpdateRequest request = DepartmentUpdateRequest.builder()
                .departmentName(DEPARTMENT_NAME)
                .locationId(null)
                .managerId(MANAGER_ID)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateDepartment(request),
                HrSchemaException.class);

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_LOCATION_ID);
    }

    @Test
    void validateEmployeeShouldNotThrowExceptionOnUpdate() {
        //Given
        DepartmentUpdateRequest request = DepartmentUpdateRequest.builder()
                .departmentName(DEPARTMENT_NAME)
                .locationId(LOCATION_ID)
                .managerId(MANAGER_ID)
                .build();

        //When
        validationService.validateDepartment(request);
    }
}