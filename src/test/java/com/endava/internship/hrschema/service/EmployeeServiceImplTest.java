package com.endava.internship.hrschema.service;

import com.endava.internship.hrschema.model.dto.EmployeeDto;
import com.endava.internship.hrschema.model.entity.EmployeeEntity;
import com.endava.internship.hrschema.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceImplTest {

    @Mock
    EmployeeRepository repository;

    @InjectMocks
    EmployeeServiceImpl service;

    @Test
    void getAllEmployees() {
        //Given
        List<EmployeeDto> expectedDtos = getEmployeeDtos();
        List<EmployeeEntity> employeesEntities = getEmployeeEntities();
        when(repository.findAll()).thenReturn(employeesEntities);

        //When
        List<EmployeeDto> actualDtos = service.getAllEmployees();

        //Then
        assertThat(actualDtos).containsExactlyInAnyOrderElementsOf(expectedDtos);

        verify(repository).findAll();
    }

    @Test
    void getEmployeeById() {
        //Given
        EmployeeDto expectedDto = getEmployeeDtos().get(0);
        EmployeeEntity employeesEntity = getEmployeeEntities().get(0);
        when(repository.findById(2L)).thenReturn(Optional.of(employeesEntity));

        //When
        EmployeeDto actualDto = service.getEmployeeById(2L);

        //Then
        assertThat(actualDto).isEqualTo(expectedDto);

        verify(repository).findById(2L);
    }

    @Test
    void createNewEmployee() {
        //Given
        EmployeeDto employeesDto = getEmployeeDtos().get(0);
        EmployeeEntity employeesEntity = getEmployeeEntities().get(0);

        //When
        service.createNewEmployee(employeesDto);

        //Then
        verify(repository).save(employeesEntity);
    }

    @Test
    void updateEmployee() {
        //Given
        EmployeeDto employeesDto = getEmployeeDtos().get(0);
        EmployeeEntity employeeEntity = getEmployeeEntities().get(0);

        //When
        service.updateEmployee(employeesDto, 2L);

        //Then
        verify(repository).save(employeeEntity);
    }

    @Test
    void deleteEmployee() {
        //When
        Long id = 2L;

        //When
        service.deleteEmployee(id);

        //Then
        verify(repository).deleteById(id);
    }

    private static List<EmployeeDto> getEmployeeDtos() {
        List<EmployeeDto> employeeDtos = new ArrayList<>();
        employeeDtos.add(EmployeeDto.builder()
                .id(2L)
                .firstName("John")
                .lastName("Doe")
                .managerId(null)
                .salary(2000.0)
                .build());
        employeeDtos.add(EmployeeDto.builder()
                .id(4L)
                .firstName("Test2")
                .lastName("Test")
                .managerId(2L)
                .salary(20000.0)
                .build());
        return employeeDtos;
    }

    private static List<EmployeeEntity> getEmployeeEntities() {
        List<EmployeeEntity> employeeEntities = new ArrayList<>();
        EmployeeEntity entity1 = new EmployeeEntity();
        entity1.setId(2L);
        entity1.setFirstName("John");
        entity1.setLastName("Doe");
        entity1.setManagerId(null);
        entity1.setSalary(2000.0);
        EmployeeEntity entity2 = new EmployeeEntity();
        entity2.setId(4L);
        entity2.setFirstName("Test2");
        entity2.setLastName("Test");
        entity2.setManagerId(entity1);
        entity2.setSalary(20000.0);
        employeeEntities.add(entity1);
        employeeEntities.add(entity2);
        return employeeEntities;
    }
}