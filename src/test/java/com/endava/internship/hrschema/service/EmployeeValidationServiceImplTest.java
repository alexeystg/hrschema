package com.endava.internship.hrschema.service;

import com.endava.internship.hrschema.exception.HrSchemaException;
import com.endava.internship.hrschema.model.request.EmployeeCreateRequest;
import com.endava.internship.hrschema.model.request.EmployeeUpdateRequest;
import org.junit.jupiter.api.Test;

import static com.endava.internship.hrschema.exception.ExceptionType.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static utils.EmployeeTestConstants.*;

class EmployeeValidationServiceImplTest {

    EmployeeValidationService validationService = new EmployeeValidationServiceImpl();

    @Test
    void validateEmployeeShouldThrowExceptionContainingInvalidFirstNameErrorOnCreate() {
        //Given
        EmployeeCreateRequest request = EmployeeCreateRequest.builder()
                .firstName("")
                .lastName(LAST_NAME)
                .email(EMAIL)
                .job(JOB)
                .phoneNumber(PHONE_NUMBER)
                .salary(SALARY)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateEmployee(request),
                HrSchemaException.class
        );

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_FIRST_NAME);
    }

    @Test
    void validateEmployeeShouldThrowExceptionContainingInvalidLastNameErrorOnCreate() {
        //Given
        EmployeeCreateRequest request = EmployeeCreateRequest.builder()
                .firstName(FIRST_NAME)
                .lastName(null)
                .email(EMAIL)
                .job(JOB)
                .phoneNumber(PHONE_NUMBER)
                .salary(SALARY)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateEmployee(request),
                HrSchemaException.class
        );

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_LAST_NAME);
    }

    @Test
    void validateEmployeeShouldThrowExceptionContainingInvalidEmailErrorOnCreate() {
        //Given
        EmployeeCreateRequest request = EmployeeCreateRequest.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email("badEmail.com")
                .job(JOB)
                .phoneNumber(PHONE_NUMBER)
                .salary(SALARY)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateEmployee(request),
                HrSchemaException.class
        );

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_EMAIL);
    }

    @Test
    void validateEmployeeShouldThrowExceptionContainingInvalidJobErrorOnCreate() {
        //Given
        EmployeeCreateRequest request = EmployeeCreateRequest.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .job(null)
                .phoneNumber(PHONE_NUMBER)
                .salary(SALARY)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateEmployee(request),
                HrSchemaException.class
        );

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_JOB);
    }

    @Test
    void validateEmployeeShouldThrowExceptionContainingInvalidPhoneNumberErrorOnCreate() {
        //Given
        EmployeeCreateRequest request = EmployeeCreateRequest.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .job(JOB)
                .phoneNumber("54156")
                .salary(SALARY)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateEmployee(request),
                HrSchemaException.class
        );

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_PHONE_NUMBER);
    }

    @Test
    void validateEmployeeShouldThrowExceptionContainingInvalidSalaryErrorOnCreate() {
        //Given
        EmployeeCreateRequest request = EmployeeCreateRequest.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .job(JOB)
                .phoneNumber(PHONE_NUMBER)
                .salary(0.3)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateEmployee(request),
                HrSchemaException.class
        );

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_SALARY);
    }

    @Test
    void validateEmployeeShouldNoyThrowExceptionOnCreate() {
        //Given
        EmployeeCreateRequest request = EmployeeCreateRequest.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .job(JOB)
                .phoneNumber(PHONE_NUMBER)
                .salary(SALARY)
                .build();

        //When
        validationService.validateEmployee(request);
    }

    @Test
    void validateEmployeeShouldThrowExceptionContainingInvalidFirstNameErrorOnUpdate() {
        //Given
        EmployeeUpdateRequest request = EmployeeUpdateRequest.builder()
                .firstName("")
                .lastName(LAST_NAME)
                .email(EMAIL)
                .job(JOB)
                .phoneNumber(PHONE_NUMBER)
                .salary(SALARY)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateEmployee(request),
                HrSchemaException.class
        );

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_FIRST_NAME);
    }

    @Test
    void validateEmployeeShouldThrowExceptionContainingInvalidLastNameErrorOnUpdate() {
        //Given
        EmployeeUpdateRequest request = EmployeeUpdateRequest.builder()
                .firstName(FIRST_NAME)
                .lastName(null)
                .email(EMAIL)
                .job(JOB)
                .phoneNumber(PHONE_NUMBER)
                .salary(SALARY)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateEmployee(request),
                HrSchemaException.class
        );

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_LAST_NAME);
    }

    @Test
    void validateEmployeeShouldThrowExceptionContainingInvalidEmailErrorOnUpdate() {
        //Given
        EmployeeUpdateRequest request = EmployeeUpdateRequest.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email("badEmail.com")
                .job(JOB)
                .phoneNumber(PHONE_NUMBER)
                .salary(SALARY)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateEmployee(request),
                HrSchemaException.class
        );

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_EMAIL);
    }

    @Test
    void validateEmployeeShouldThrowExceptionContainingInvalidJobErrorOnUpdate() {
        //Given
        EmployeeUpdateRequest request = EmployeeUpdateRequest.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .job(null)
                .phoneNumber(PHONE_NUMBER)
                .salary(SALARY)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateEmployee(request),
                HrSchemaException.class
        );

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_JOB);
    }

    @Test
    void validateEmployeeShouldThrowExceptionContainingInvalidPhoneNumberErrorOnUpdate() {
        //Given
        EmployeeUpdateRequest request = EmployeeUpdateRequest.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .job(JOB)
                .phoneNumber("54156")
                .salary(SALARY)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateEmployee(request),
                HrSchemaException.class
        );

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_PHONE_NUMBER);
    }

    @Test
    void validateEmployeeShouldThrowExceptionContainingInvalidSalaryErrorOnUpdate() {
        //Given
        EmployeeUpdateRequest request = EmployeeUpdateRequest.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .job(JOB)
                .phoneNumber(PHONE_NUMBER)
                .salary(0.3)
                .build();

        //When
        HrSchemaException expectedException = catchThrowableOfType(
                () -> validationService.validateEmployee(request),
                HrSchemaException.class
        );

        //Then
        assertThat(expectedException.getExceptionType()).isEqualTo(INVALID_SALARY);
    }

    @Test
    void validateEmployeeShouldNoyThrowExceptionOnUpdate() {
        //Given
        EmployeeUpdateRequest request = EmployeeUpdateRequest.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .job(JOB)
                .phoneNumber(PHONE_NUMBER)
                .salary(SALARY)
                .build();

        //When
        validationService.validateEmployee(request);
    }
}