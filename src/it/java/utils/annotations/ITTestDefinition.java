package utils.annotations;


import com.endava.internship.hrschema.HrSchemaApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import utils.db.ITPrerequisites;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SpringBootTest(classes = HrSchemaApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("it")
@SpringBootConfiguration
@Retention(RetentionPolicy.RUNTIME)
@Import(ITPrerequisites.class)
public @interface ITTestDefinition {
}
