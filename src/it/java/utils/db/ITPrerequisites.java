package utils.db;

import com.endava.internship.hrschema.model.entity.DepartmentEntity;
import com.endava.internship.hrschema.model.entity.EmployeeEntity;
import com.endava.internship.hrschema.repository.DepartmentRepository;
import com.endava.internship.hrschema.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@TestConfiguration
@AllArgsConstructor
public class ITPrerequisites {

    private final EmployeeRepository employeeRepository;

    private final DepartmentRepository departmentRepository;

    public void clear() {
        employeeRepository.deleteAll();
        departmentRepository.deleteAll();
    }

    @Transactional
    public EmployeeEntity createEmployeeEntity(
            String firstName,
            String lastName,
            String email,
            String phoneNumber,
            String hireDate,
            String job,
            double salary,
            EmployeeEntity managerId,
            DepartmentEntity departmentId
    ) {
        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setFirstName(firstName);
        employeeEntity.setLastName(lastName);
        employeeEntity.setEmail(email);
        employeeEntity.setPhoneNumber(phoneNumber);
        employeeEntity.setHireDate(hireDate);
        employeeEntity.setJob(job);
        employeeEntity.setSalary(salary);
        employeeEntity.setCommissionPct(0.2f);
        employeeEntity.setManagerId(managerId);
        employeeEntity.setDepartmentId(departmentId);
        employeeRepository.save(employeeEntity);
        return employeeEntity;
    }

    @Transactional
    public DepartmentEntity createDepartmentEntity(String departmentName, EmployeeEntity managerId, long locationId) {
        DepartmentEntity departmentEntity = new DepartmentEntity();
        departmentEntity.setDepartmentName(departmentName);
        departmentEntity.setManagerId(managerId);
        departmentEntity.setLocationId(locationId);
        departmentRepository.save(departmentEntity);
        return departmentEntity;
    }

    @Transactional(readOnly = true)
    public List<DepartmentEntity> findAllDepartments(){
        return departmentRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<EmployeeEntity> findAllEmployees(){
        return employeeRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<DepartmentEntity> findDepartmentById(long id){
        return departmentRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public Optional<EmployeeEntity> findEmployeeById(long id){
        return employeeRepository.findById(id);
    }
}