package controller;

import com.endava.internship.hrschema.exception.ExceptionType;
import com.endava.internship.hrschema.model.entity.DepartmentEntity;
import com.endava.internship.hrschema.model.entity.EmployeeEntity;
import com.endava.internship.hrschema.model.request.EmployeeCreateRequest;
import com.endava.internship.hrschema.model.request.EmployeeUpdateRequest;
import com.endava.internship.hrschema.model.response.EmployeeExpandedResponse;
import com.endava.internship.hrschema.model.response.EmployeeResponse;
import com.endava.internship.hrschema.model.response.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import utils.annotations.ITTestDefinition;
import utils.db.ITPrerequisites;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ITTestDefinition
class TestEmployeeControllerIT {

    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Autowired
    private ITPrerequisites prerequisites;

    @LocalServerPort
    private int port;

    private String URI;

    @BeforeEach
    void setUp() {
        URI = "http://localhost:" + port + "/api/employees";
    }

    @AfterEach
    void cleanUp() {
        prerequisites.clear();
    }

    @Test
    void findByIdNotExpanded() {
        //Given
        DepartmentEntity department = prerequisites.createDepartmentEntity("Sales", null, 10L);
        EmployeeResponse expectedResponse = new EmployeeResponse("John", "Doe", department.getId());

        EmployeeEntity manager = prerequisites.createEmployeeEntity(
                "John",
                "Doe",
                "doe@gmail.com",
                "012345678",
                "2006-06-01",
                "test",
                5000.0,
                null,
                department
        );

        //When
        ResponseEntity<EmployeeResponse> actualEntity = restTemplate.getForEntity(
                URI + "/{id}",
                EmployeeResponse.class,
                manager.getId()
        );

        //Then
        assertThat(actualEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualEntity.getBody()).isEqualTo(expectedResponse);
    }

    @Test
    void findByIdExpanded() {
        //Given
        DepartmentEntity department = prerequisites.createDepartmentEntity("Sales", null, 10L);
        EmployeeExpandedResponse expectedResponse = new EmployeeExpandedResponse(
                "John",
                "Doe",
                "doe@gmail.com",
                "012345678",
                "2006-06-01",
                "test",
                5000.0,
                0.2f,
                null,
                department.getId()
        );

        EmployeeEntity manager = prerequisites.createEmployeeEntity(
                "John",
                "Doe",
                "doe@gmail.com",
                "012345678",
                "2006-06-01",
                "test",
                5000.0,
                null,
                department
        );


        //When
        ResponseEntity<EmployeeExpandedResponse> responseEntity = restTemplate.getForEntity(
                URI + "/{id}?expand=true",
                EmployeeExpandedResponse.class,
                manager.getId()
        );

        //Then
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isEqualTo(expectedResponse);
    }

    @Test
    void findAllNotExpanded() {
        //Given
        DepartmentEntity department = prerequisites.createDepartmentEntity("Sales", null, 10L);
        List<EmployeeResponse> expectedResponses = Arrays.asList(
                new EmployeeResponse("John", "Doe", department.getId()),
                new EmployeeResponse("Kate", "Smith", department.getId())
        );

        EmployeeEntity manager = prerequisites.createEmployeeEntity(
                "John",
                "Doe",
                "doe@gmail.com",
                "012345678",
                "2006-06-01",
                "test",
                5000.0,
                null,
                department
        );
        prerequisites.createEmployeeEntity(
                "Kate",
                "Smith",
                "smith@gmail.com",
                "021345678",
                "2006-07-01",
                "salesman",
                4500.0,
                manager,
                department
        );

        //When
        ResponseEntity<EmployeeResponse[]> actualEntity = restTemplate.getForEntity(
                URI + "/",
                EmployeeResponse[].class
        );

        //Then
        assertThat(actualEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualEntity.getBody()).containsExactlyInAnyOrderElementsOf(expectedResponses);
    }

    @Test
    void findAllExpanded() {
        //Given
        DepartmentEntity department = prerequisites.createDepartmentEntity("Sales", null, 10L);
        EmployeeEntity manager = prerequisites.createEmployeeEntity(
                "John",
                "Doe",
                "doe@gmail.com",
                "012345678",
                "2006-06-01",
                "test",
                5000.0,
                null,
                department
        );

        prerequisites.createEmployeeEntity(
                "Kate",
                "Smith",
                "smith@gmail.com",
                "021345678",
                "2006-07-01",
                "salesman",
                4500.0,
                manager,
                department
        );

        List<EmployeeExpandedResponse> expectedResponses = Arrays.asList(
                new EmployeeExpandedResponse(
                        "John",
                        "Doe",
                        "doe@gmail.com",
                        "012345678",
                        "2006-06-01",
                        "test",
                        5000.0,
                        0.2f,
                        null,
                        department.getId()
                ), new EmployeeExpandedResponse(
                        "Kate",
                        "Smith",
                        "smith@gmail.com",
                        "021345678",
                        "2006-07-01",
                        "salesman",
                        4500.0,
                        0.2f,
                        manager.getId(),
                        department.getId()
                )
        );

        //When
        ResponseEntity<EmployeeExpandedResponse[]> actualEntity = restTemplate.getForEntity(
                URI + "/?expand=true",
                EmployeeExpandedResponse[].class
        );

        //Then
        assertThat(actualEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualEntity.getBody()).containsExactlyInAnyOrderElementsOf(expectedResponses);
    }

    @Test
    void createEmployee() {
        //Given
        assertThat(prerequisites.findAllEmployees()).isEmpty();
        EmployeeCreateRequest employee = EmployeeCreateRequest.builder()
                .firstName("Test")
                .lastName("Test")
                .email("test@gmail.com")
                .phoneNumber("012345679")
                .hireDate("2010-10-10")
                .job("Salesman")
                .salary(5000.0)
                .commissionPct(0.2f)
                .managerId(null)
                .departmentId(null)
                .build();
        EmployeeEntity expectedEmployee = new EmployeeEntity();
        expectedEmployee.setFirstName("Test");
        expectedEmployee.setLastName("Test");
        expectedEmployee.setEmail("test@gmail.com");
        expectedEmployee.setPhoneNumber("012345679");
        expectedEmployee.setHireDate("2010-10-10");
        expectedEmployee.setJob("Salesman");
        expectedEmployee.setSalary(5000.0);
        expectedEmployee.setCommissionPct(0.2f);

        //When
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(URI + "/", employee, String.class);

        //Then
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(prerequisites.findAllEmployees())
                .usingElementComparatorIgnoringFields("id")
                .containsExactly(expectedEmployee);
    }

    @Test
    void createEmployeeValidationTest() {
        //Given
        EmployeeCreateRequest employee = EmployeeCreateRequest.builder()
                .firstName("Test")
                .lastName("Test")
                .email("testgmail.com")
                .phoneNumber("012345679")
                .hireDate("2010-10-10")
                .job("Salesman")
                .salary(5000.0)
                .commissionPct(0.2f)
                .managerId(null)
                .departmentId(null)
                .build();
        ExceptionType expectedType = ExceptionType.INVALID_EMAIL;
        ErrorResponse expectedBody = new ErrorResponse(
                expectedType.name(),
                expectedType.getMessage()
        );

        //When
        ResponseEntity<ErrorResponse> responseEntity = restTemplate.postForEntity(
                URI + "/",
                employee,
                ErrorResponse.class
        );
        ErrorResponse actualBody = responseEntity.getBody();

        //Then
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(actualBody).isNotNull();
        assertThat(actualBody.getErrorType()).isEqualTo(expectedBody.getErrorType());
        assertThat(actualBody.getMessage()).isEqualTo(expectedBody.getMessage());
    }

    @Test
    void deleteById() {
        //Given
        EmployeeEntity employee = prerequisites.createEmployeeEntity(
                "John",
                "Doe",
                "doe@gmail.com",
                "012345678",
                "2006-06-01",
                "test",
                5000.0,
                null,
                null
        );
        long id = employee.getId();

        //When
        restTemplate.delete(URI + "/" + id);

        //Then
        assertThat(prerequisites.findEmployeeById(id)).isEmpty();
    }

    @Test
    void updateById() {
        //Given
        EmployeeEntity createdEmployee = prerequisites.createEmployeeEntity(
                "John",
                "Doe",
                "doe@gmail.com",
                "012345678",
                "2006-06-01 00:00:00",
                "test",
                5000.0,
                null,
                null
        );
        long id = createdEmployee.getId();
        EmployeeUpdateRequest employeeUpdateRequest = EmployeeUpdateRequest.builder()
                .firstName("Test3")
                .lastName("Test4")
                .email("test@gmail.com")
                .phoneNumber("012345679")
                .hireDate("2006-06-01")
                .job("Salesman")
                .salary(5000.0)
                .commissionPct(0.2f)
                .managerId(null)
                .departmentId(null)
                .build();

        EmployeeEntity expectedEmployee = new EmployeeEntity();
        expectedEmployee.setId(id);
        expectedEmployee.setFirstName("Test3");
        expectedEmployee.setLastName("Test4");
        expectedEmployee.setEmail("test@gmail.com");
        expectedEmployee.setPhoneNumber("012345679");
        expectedEmployee.setHireDate("2006-06-01");
        expectedEmployee.setJob("Salesman");
        expectedEmployee.setSalary(5000.0);
        expectedEmployee.setCommissionPct(0.2f);
        expectedEmployee.setManagerId(null);
        expectedEmployee.setDepartmentId(null);

        //When
        restTemplate.put(URI + "/" + id, employeeUpdateRequest);
        Optional<EmployeeEntity> actualEmployee = prerequisites.findEmployeeById(id);

        //Then
        assertThat(actualEmployee).hasValue(expectedEmployee);
    }
}