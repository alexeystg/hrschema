package controller;

import com.endava.internship.hrschema.exception.ExceptionType;
import com.endava.internship.hrschema.exception.HrSchemaException;
import com.endava.internship.hrschema.model.entity.DepartmentEntity;
import com.endava.internship.hrschema.model.request.DepartmentCreateRequest;
import com.endava.internship.hrschema.model.request.DepartmentUpdateRequest;
import com.endava.internship.hrschema.model.response.DepartmentExpandedResponse;
import com.endava.internship.hrschema.model.response.DepartmentResponse;
import com.endava.internship.hrschema.model.response.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import utils.annotations.ITTestDefinition;
import utils.db.ITPrerequisites;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ITTestDefinition
class TestDepartmentControllerIT {

    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Autowired
    private ITPrerequisites prerequisites;

    @LocalServerPort
    private int port;

    private String URI;

    @BeforeEach
    void generateUri() {
        URI = "http://localhost:" + port + "/api/departments";
    }

    @AfterEach
    void cleanUp() {
        prerequisites.clear();
    }

    @Test
    void findByIdNotExpanded() {
        //Given
        DepartmentEntity department = prerequisites.createDepartmentEntity("Sales", null, 10L);
        DepartmentResponse expectedResponse = new DepartmentResponse("Sales");

        //When
        ResponseEntity<DepartmentResponse> actualEntity = restTemplate.getForEntity(
                URI + "/{id}",
                DepartmentResponse.class,
                department.getId()
        );

        //Then
        assertThat(actualEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualEntity.getBody()).isEqualTo(expectedResponse);
    }

    @Test
    void findByIdExpanded() {
        //Given
        DepartmentEntity department = prerequisites.createDepartmentEntity("Sales", null, 10L);
        DepartmentExpandedResponse expectedResponse = new DepartmentExpandedResponse("Sales", null, 10L);
        //When
        ResponseEntity<DepartmentExpandedResponse> actualEntity = restTemplate.getForEntity(
                URI + "/{id}" + "?expand=true",
                DepartmentExpandedResponse.class,
                department.getId()
        );

        //Then
        assertThat(actualEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualEntity.getBody()).isEqualTo(expectedResponse);
    }

    @Test
    void findAllNotExpanded() {
        //Given
        prerequisites.createDepartmentEntity("Sales", null, 10L);
        prerequisites.createDepartmentEntity("Logistics", null, 20L);
        List<DepartmentResponse> expectedResponses = Arrays.asList(
                new DepartmentResponse("Sales"),
                new DepartmentResponse("Logistics")
        );

        //When
        ResponseEntity<DepartmentResponse[]> actualEntity = restTemplate.getForEntity(
                URI + "/",
                DepartmentResponse[].class
        );

        //Then
        assertThat(actualEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualEntity.getBody()).containsExactlyInAnyOrderElementsOf(expectedResponses);
    }

    @Test
    void findAllExpanded() {
        //Given
        prerequisites.createDepartmentEntity("Sales", null, 10L);
        prerequisites.createDepartmentEntity("Logistics", null, 20L);
        List<DepartmentExpandedResponse> expectedResponses = Arrays.asList(
                new DepartmentExpandedResponse("Sales", null, 10L),
                new DepartmentExpandedResponse("Logistics", null, 20L)
        );

        //When
        ResponseEntity<DepartmentExpandedResponse[]> actualEntity = restTemplate.getForEntity(
                URI + "?expand=true",
                DepartmentExpandedResponse[].class
        );

        //Then
        assertThat(actualEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualEntity.getBody()).containsExactlyInAnyOrderElementsOf(expectedResponses);
    }

    @Test
    void createDepartment() {
        //Given
        assertThat(prerequisites.findAllDepartments()).isEmpty();
        DepartmentCreateRequest department = DepartmentCreateRequest.builder()
                .departmentName("Test")
                .managerId(null)
                .locationId(10L)
                .build();
        DepartmentEntity expectedDepartment = new DepartmentEntity();
        expectedDepartment.setDepartmentName("Test");
        expectedDepartment.setManagerId(null);
        expectedDepartment.setLocationId(10L);

        //When
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                URI + "/",
                department,
                String.class
        );

        //Then
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(prerequisites.findAllDepartments())
                .usingElementComparatorIgnoringFields("id")
                .containsExactly(expectedDepartment);
    }

    @Test
    void createDepartmentValidationTest() {
        //Given
        DepartmentCreateRequest department = DepartmentCreateRequest.builder()
                .departmentName(null)
                .managerId(null)
                .locationId(10L)
                .build();
        ExceptionType expectedType = ExceptionType.INVALID_DEPARTMENT_NAME;
        ErrorResponse expectedBody = new ErrorResponse(
                expectedType.name(),
                expectedType.getMessage()
        );

        //When
        ResponseEntity<ErrorResponse> responseEntity = restTemplate.postForEntity(
                URI + "/",
                department,
                ErrorResponse.class
        );
        ErrorResponse body = responseEntity.getBody();

        //Then
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(body).isNotNull();
        assertThat(body.getErrorType()).isEqualTo(expectedBody.getErrorType());
        assertThat(body.getMessage()).isEqualTo(expectedBody.getMessage());
    }

    @Test
    void deleteById() {
        //Given
        DepartmentEntity department = prerequisites.createDepartmentEntity("Sales", null, 10L);
        long id = department.getId();

        //When
        restTemplate.delete(URI + "/" + id);

        //Then
        assertThat(prerequisites.findDepartmentById(id)).isEmpty();
    }

    @Test
    void updateById() {
        //Given
        DepartmentEntity createdDepartment = prerequisites.createDepartmentEntity("Sales", null, 10L);
        long id = createdDepartment.getId();
        DepartmentUpdateRequest employeeUpdateRequest = DepartmentUpdateRequest.builder()
                .departmentName("Test3")
                .managerId(null)
                .locationId(10L)
                .build();

        DepartmentEntity expectedDepartment = new DepartmentEntity();
        expectedDepartment.setId(id);
        expectedDepartment.setDepartmentName("Test3");
        expectedDepartment.setManagerId(null);
        expectedDepartment.setLocationId(10L);

        //When
        restTemplate.put(URI + "/" + id, employeeUpdateRequest);
        Optional<DepartmentEntity> actualDepartment = prerequisites.findDepartmentById(id);

        //Then
        assertThat(actualDepartment).hasValue(expectedDepartment);
    }
}